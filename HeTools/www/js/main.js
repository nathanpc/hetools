// main.js
// Main functions

function loaded() {
	document.addEventListener("deviceready", onDeviceReady, false);
	loadBookmarks();
	bookmarks = $("#bookmarkList > li").length + 1 - $.jStorage.get("deleted", 0);
	metDb = $.jStorage.get("metric", "meters");
	scrollBookmarks = new iScroll('Bookmarks', { vScrollbar: true, hideScrollbar: true, fadeScrollbar: true });
	scrollSettings = new iScroll('Settings', { vScrollbar: true, hideScrollbar: true, fadeScrollbar: true });
	
	if (metDb == "miles") {
		setMetric("miles");
	} else if (metDb == "kilometers") {
		setMetric("kilometers");
	} else if (metDb == "meters") {
		setMetric("meters");
	}
}

function onDeviceReady() {
	// Initializating TabBar
	nativeControls = window.plugins.nativeControls;
	nativeControls.createTabBar();
	
	// Map tab
	nativeControls.createTabBarItem(
		"maps",
		"Map",
		"/www/tabs/map.png",
		{"onSelect": function() {
			switchToSectionWithId('Home');
		}}
	);
	
	// Bookmarks tab
	nativeControls.createTabBarItem(
		"bookmarks",
		"Bookmarks",
		"tabButton:Bookmarks",
		{"onSelect": function() {
			loadBookmarks();
			switchToSectionWithId('Bookmarks');
			setTimeout(function () {
				scrollBookmarks.refresh();
			}, 0);
		}}
	);
	
	// Settings tab
	nativeControls.createTabBarItem(
		"settings",
		"Settings",
		"tabButton:More",
		{"onSelect": function() {
			switchToSectionWithId('Settings');
			loadSettings();
			setTimeout(function () {
				scrollSettings.refresh();
			}, 0);
		}}
	);
	
	// Compile the TabBar
	nativeControls.showTabBar();
	nativeControls.showTabBarItems("maps", "bookmarks", "settings");

	document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
	nativeControls.selectTabBarItem("maps");
	switchToSectionWithId('Home');
	navigator.geolocation.getCurrentPosition(loadMap, function(error) {
		// Handle Error
		navigator.notification.alert(
		    error.message,
		    function() {
				// Do nothing
			},
		    'Error',
		    'Done'
		);
	});
}

function loadMap(position) {
	var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	redPin = new google.maps.MarkerImage("images/red_pin.png", null, null, null, new google.maps.Size(15, 35));
	greenPin = new google.maps.MarkerImage("images/green_pin.png", null, null, null, new google.maps.Size(15, 35));
	
	var myOptions = {
		center: latLng,
		zoom: 8,
		mapTypeId: google.maps.MapTypeId.TERRAIN,
		panControl: false,
		zoomControl: false,
		mapTypeControl: true,
		scaleControl: false,
		streetViewControl: false,
		overviewMapControl: false
	};
	
	map = new google.maps.Map(document.getElementById('mapCanvas'), myOptions);
	
	google.maps.event.addListener(map, "click", function(event) {
		clickLatlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
		placeMarker(clickLatlng);
	});	
}

function calcDist() {
	var dist = null;

	// Use computeHeading for multiple markers http://code.google.com/intl/el/apis/maps/documentation/javascript/geometry.html#Distance
	
	clearTimeout(calcTimer);
	
	if (localStorage.getItem("metric") == "miles") {
		dist = google.maps.geometry.spherical.computeDistanceBetween(marker.position, marker2.position) * 0.00062137119 + " Miles";
	} else if (localStorage.getItem("metric") == "kilometers") {
		dist = google.maps.geometry.spherical.computeDistanceBetween(marker.position, marker2.position) / 1000 + " Kilometers";
	} else if (localStorage.getItem("metric") == "meters") {
		dist = google.maps.geometry.spherical.computeDistanceBetween(marker.position, marker2.position) + " Meters";
	}

	navigator.notification.alert(
	    dist,
	    function() {
			// Do nothing
		},
	    'Distance',
	    'Done'
	);
	
	var cords = [
		marker.position,
		marker2.position
	];
	
	var path = new google.maps.Polyline({
		path: cords,
		strokeColor: "#FF0000",
		strokeOpacity: 0.7,
		strokeWeight: 4
	});
	path.setMap(map);
}

// Notification functions

function showMarkerDialog(location, marker) {
	// Add a place name if it's on favorites
	locationString = location.toString();
	var locStr = locationString.replace("(", "Lat: ");
	locStr = locStr.replace(")", "");
	locStr = locStr.replace(", ", "\nLong: ");
	selectedMarker = marker;
	
	navigator.notification.confirm(
		locStr,
		onSelectMarkerDialog,
		"Place Name",
		"Bookmark,Clear Markers,Dismiss"
	);
}

function onSelectMarkerDialog(button) {
	var locationStr = locationString;  // MAKE IT COMPACTIBLE WITH MARKER2
	var locaStr = locationStr.replace("(", "");
	locaStr = locaStr.replace(")", "");
	locaStr = locaStr.replace(", ", ",");
	var locArr = locaStr.split(",");
	
	if (button == 1) {
		var bookName = prompt("Bookmark Name", "");
		if (bookName !== null && bookName !== "") {
			// Valid bookmark name
			var json = { "name": bookName, "lat": locArr[0].toString(), "lng": locArr[1].toString() };
			var jsString = JSON.stringify(json);
			
			$.jStorage.set(genBookmark(bookmarks++), jsString);
		} else {
			// Invalid bookmark name
		}
	} else if (button == 2) {
		// delete marker
		if (selectedMarker == "marker") {
			marker.setMap(null);
			marker2.setMap(null);
			mknum = 0;
		} else if (selectedMarker == "marker2") {
			marker.setMap(null);
			marker2.setMap(null);
			mknum = 0;
		} else {
			navigator.notification.alert(
			    "Something went wrong!",
			    function() {
					// Do nothing
				},
			    'Error',
			    'Done'
			);
		}
	} else if (button == 3) {
		// Do nothing
	} else {
		navigator.notification.alert(
		    "Something went wrong with your choice",
		    function() {
				// Do nothing
			},
		    'Error',
		    'Done'
		);
	}	
}

function genBookmark(i) {
	var appName = "HeTools";
	return appName + i;
}

function loadBookmarks() {
	var arr = $.jStorage.index();
	var listLength = $("#bookmarkList > li").length;
	
	$("#bookmarkList > li").remove();
	$.each(arr, function(key, value) {
		$("#bookmarkList").prepend("<li class='" + value + "'><a href='#" + value + "' onClick='placeBookmark(\"" + value + "\")'>" + showBookmarkName(value) + "</a></li>");
	});
}

function showBookmarkName(id) {
	var json = $.parseJSON($.jStorage.get(id));
	return json.name;
}

function placeBookmark(id) {
	var json = $.parseJSON($.jStorage.get(id));
	var lat = json.lat;
	var lng = json.lng;

	var latLng = new google.maps.LatLng(lat, lng);
	switchToSectionWithId('Home');
	nativeControls.selectTabBarItem("maps");
	placeMarker(latLng);
}

function placeMarker(latLng) {
	if (mknum == 0) {
		marker = new google.maps.Marker({
			position: latLng,
			map: map,
			draggable: true,
			animation: google.maps.Animation.DROP,
			title: 'Marker 1',
			icon: redPin
		});
		
		google.maps.event.addListener(marker, "click", function() {
			navigator.notification.vibrate();
			showMarkerDialog(marker.position, "marker");
		});
		
		mknum = 1;
	} else if(mknum == 1) {
		marker2 = new google.maps.Marker({
			position: latLng,
			map: map,
			draggable: true,
			animation: google.maps.Animation.DROP,
			title: 'Marker 2',
			icon: greenPin
		});
		
		google.maps.event.addListener(marker2, "click", function() {
			navigator.notification.vibrate();
			showMarkerDialog(marker2.position, "marker2");
		});
		
		mknum = 2;
	} else {
		navigator.notification.alert(
		    "Only 2 markers please!",
		    function() {
				// Do nothing
			},
		    'Error',
		    'Done'
		);
	}
}