// settings.js
// Settings functions

function loadSettings() {
	if (localStorage.getItem("metric") == "miles") {
		// Do miles convertion
		setMetric("miles");
	} else if (localStorage.getItem("metric") == "kilometers") {
		// Do km convertion
		setMetric("kilometers");
	} else if (localStorage.getItem("metric") == "meters") {
		// stay the same
		setMetric("meters");
	}
}

function setMetric(metric) {
	removeLabelSelection();
	
	if (metric == "miles") {
		localStorage.setItem("metric", "miles");
		$("#lblMiles").addClass("lblSelected");
	} else if (metric == "kilometers") {
		localStorage.setItem("metric", "kilometers");
		$("#lblKilometers").addClass("lblSelected");
	} else {
		localStorage.setItem("metric", "meters");
		$("#lblMeters").addClass("lblSelected");
	}
}